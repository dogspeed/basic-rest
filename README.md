# Basic rest
> Basic Go API Rest

This Go program creates a basic API rest based on Elliot Forbes article:
[Creating a RESTful API With Golang](https://tutorialedge.net/golang/creating-restful-api-with-golang/)

This API provides the following endpoints:
* / GET - Home page
* /articles GET - returns all articles
* /article POST - creates a new article
* /article PUT - updates an existing article
* /article/{id} GET - returns one article
* /article/{id} DELETE - deletes one article